# syntax=docker/dockerfile:1

# use latest vanilla Alpine Linux
FROM alpine:latest
## install packages:
## - [ca-certificates](https://pkgs.alpinelinux.org/packages?name=ca-certificates)
## - [sqlite](https://pkgs.alpinelinux.org/packages?name=sqlite) providing the `sqlite3` CLI
RUN apk add --no-cache \
  ca-certificates \
  sqlite

# set up Gatus 
## copy Gatus binary from latest stable release
COPY --link --from=twinproduction/gatus:stable /gatus /gatus
## add our config file and public folder
COPY --link config.yaml /config/config.yaml
COPY --link public /public
## set necessary config
ENV PORT=8080
EXPOSE ${PORT}
ENTRYPOINT ["/gatus"]

## ignored by Fly
VOLUME [ "/data" ]
