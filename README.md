# VoteLog status page

This repository contains code and configuration related to the VoteLog status page available under [**`status.votelog.ch`**](https://status.votelog.ch/).

We rely on [Gatus](https://gatus.io/) which we host on [Fly](https://fly.io/).

## Gatus configuration

Gatus is configured via the [`config.yaml`](config.yaml) file. Sensitive configuration data is stored in [Fly secrets](https://fly.io/docs/reference/secrets/).
Currently this includes the following environment variables:

-   `SMTP_USERNAME`
-   `SMTP_PASSWORD`
-   `BASIC_AUTH_TOKEN`

## Hosting details

The Fly [app](https://fly.io/docs/reference/apps/) is named `votelog-status` and the [persistent storage volume](https://fly.io/docs/reference/volumes/) is
named `votelog_status` with a current size of 1 GiB[^readme-1]. Its unique ID is `vol_2yxp4ml53o3v63qd`.

Currently, we use an [SQLite](https://en.wikipedia.org/wiki/SQLite) database and run only a single [`shared-cpu-1x` instance with `256 MB`
RAM](https://fly.io/docs/about/pricing/#compute) hosted in the [*Frankfurt, Germany* region](https://fly.io/docs/reference/regions/). Should it prove necessary
to expand performance for our main users (located in Switzerland), we could [increase RAM](https://fly.io/docs/flyctl/scale-memory/) and/or [switch to a faster
CPU](https://fly.io/docs/flyctl/scale-vm/) anytime[^readme-2].

Should we also want to provide fast access for non-European users, we could [run multiple instances](https://fly.io/docs/apps/scale-count/) of Gatus in multiple
regions. This would either require to replicate the SQLite database using [LiteFS](https://fly.io/docs/litefs/), or to switch to
[PostgreSQL](https://fly.io/docs/getting-started/multi-region-databases/). However, this most probably won't be necessary in the forseeable future.

[^readme-1]: The volume size can always be [extended](https://fly.io/docs/flyctl/volumes-extend/). To extend it to 5 GiB for example, simply run:

    ``` sh
    flyctl volumes extend vol_2yxp4ml53o3v63qd --size=5
    ```

    Note that this will restart the app.

[^readme-2]: Note that scaling automatically restarts the app. The most relevant documentation on (auto)scaling Fly apps includes:

    -   [Scale Machine CPU and RAM](https://fly.io/docs/apps/scale-machine/)
    -   [Scale the Number of Machines](https://fly.io/docs/apps/scale-count/)
    -   [Automatically Stop and Start Machines](https://fly.io/docs/apps/autostart-stop/)

## Admin access

We can directly connect into the root filesystem of the Fly app via an [SSH tunnel](https://fly.io/docs/flyctl/ssh-console/) to perform any low-level
administration tasks or directly access the database. The SQLite database file is stored under `/data/data.db`.

To directly log into the database using the [SQLite CLI](https://www.sqlite.org/cli.html) over SSH, run:

``` sh
flyctl ssh console -C 'sqlite3 /data/data.db'
```

To exit the SQLite CLI, press <kbd>Ctrl</kbd>+<kbd>D</kbd>, and then enter `exit` to terminate the SSH tunnel.

We can also fetch a copy of the SQLite database to inspect it locally[^readme-3] using SFTP:

``` sh
flyctl sftp get /data/data.db /PATH/TO/LOCAL_FILE.db
```

[^readme-3]: E.g. using [DB Browser for SQLite](https://sqlitebrowser.org/).

## License

Code and configuration in this repository are licensed under [`AGPL-3.0-or-later`](https://spdx.org/licenses/AGPL-3.0-or-later.html). See
[LICENSE.md](LICENSE.md).

----------------------------------------------------------------------------------------------------------------------------------------------------------------
